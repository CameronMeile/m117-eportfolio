# Kapitel 1 | Netzwerkverkabelung (Ethernet)

[PDF Teil 1 | Kabel](Kapitel 1 | Netzwerkverkabelung/Teil1_Kabel.pdf)

INDEX
- [1.0 Simplex, Halfduplex und Duplex](#10-simplex-halfduplex-und-duplex)
    - [1.1 Netzwerktopologie](#11-netzwerktopologie)
- [2.0 Das Ethernetkabel](#20-das-ethernetkabel)
    - [2.1 Draht, Litze oder Glasfaser](#21-draht-litze-oder-glasfaser)
    - [2.2 Konzepte zur Abwehr von Störeinflüssen](#22-konzepte-zur-abwehr-von-störeinflüssen)
    - [2.3 Die Ethernet-Kabelkategorien](#23-die-ethernet-kabelkategorien)
    - [2.4 Die Ethernet-Medientypen](#24-die-ethernet-medientypen)
    - [2.5 Ethernetkabel im Eigenbau mit RJ45](#25-ethernetkabel-im-eigenbau-mit-rj45)
- [3.0 Universelle Gebäudeverkabelung](#30-universelle-gebäudeverkabelung)
    - [3.1 Das Logisches Layout und der Verkabelungsplan](#31-das-logisches-layout-und-der-verkabelungsplan)

# 1.0 Simplex, Halfduplex und Duplex

Damit werden die verschiedenen Arten der Punkt-zu-Punkt Datenübertragung und Richtungsabhängigkeit von Kommunikationskanälen beschrieben. Dazu soll im folgenden jeweils eine Analogie aus der Eisenbahnwelt und ein Praxisbeispiel dienen:

### 1-Kanal Simplex → Unidirektiona
- 1-Kanal Simplex beschreibt eine drahtlose Kommunikation
    - Daten werden über einen einzigen Kanal in nur eine Richtung übertragen
    - Empfänger kann keine Daten an den Sender zurückschicken
    - Kann in Anwendungen wie Rundfunk, Fernüberwachung von Maschinen oder drahtloser Steuerung von Geräten verwendet werden
    - Keine Rückmeldung möglich, um Datenempfang oder Fehlerbehebung zu bestätigen
Möglicherweise höhere Übertragungsrate, da keine Zeit für Rückmeldungen aufgewendet werden muss.

![1-Kanal Simplex → Unidirektional](https://www.juergarnold.ch/Netzwerkverkabelung/verbindung1.jpg)

### 1-Kanal Half-Duplex → Bidirektional

- 1-Kanal Half-Duplex ermöglicht bidirektionale Kommunikation
    - Datenübertragungen erfolgen über einen einzelnen Kanal
    - Geräte können gleichzeitig senden und empfangen, aber nicht gleichzeitig
    - Verwendet in historischen Ethernetverkabelungen wie 10Base5 und "Yellow-Cable", sowie in Hubs
    - Rückmeldungen und Fehlerbehebung möglich
    - Kollisionen möglich, wenn beide Geräte gleichzeitig Daten senden möchten
    - Effektive Methode für die drahtgebundene Kommunikation in Netzwerken und anderen Anwendungen, bei denen bidirektionale Kommunikation erforderlich ist.

![1-Kanal Half-Duplex → Bidirektional (z.B.: Historische Ethernetverkabelung 10Base5, Yellow-Cable, Hub etc.)](https://www.juergarnold.ch/Netzwerkverkabelung/verbindung2.jpg)

### 2-Kanal (Voll)-Duplex → Bidirektional

- 2-Kanal Voll-Duplex ermöglicht gleichzeitige bidirektionale Kommunikation
    - Datenübertragungen erfolgen über zwei separate Kanäle
    - Geräte können gleichzeitig senden und empfangen, ohne Kollisionen zu verursachen
    - Verwendet in Ethernetverkabelungen wie 10BaseT mit Kabel der Kategorie CAT3
    - Bietet höhere Bandbreite und geringere Latenzzeit als Half-Duplex
    - Effektive Methode für schnelle und zuverlässige bidirektionale Kommunikation in Netzwerken und anderen Anwendungen.

![2-Kanal (Voll)-Duplex → Bidirektional (z.B.: Ethernet 10BaseT mit Kabel der Kategorie CAT3)](https://www.juergarnold.ch/Netzwerkverkabelung/verbindung3.jpg)

### 4-Kanal (Voll)-Duplex → Bidirektional

- 4-Kanal Voll-Duplex ermöglicht gleichzeitige bidirektionale Kommunikation
    - Datenübertragungen erfolgen über vier separate Kanäle
    - Geräte können gleichzeitig senden und empfangen, ohne Kollisionen zu verursachen
    - Verwendet in Ethernetverkabelungen wie 1000BaseT mit Kabel der Kategorie CAT5e
    - Bietet höhere Bandbreite und geringere Latenzzeit als 2-Kanal Voll-Duplex
    - Effektive Methode für schnelle und zuverlässige bidirektionale Kommunikation in Netzwerken und anderen Anwendungen, die eine höhere Bandbreite erfordern.

![4-Kanal (Voll)-Duplex → Bidirektional (z.B.: Ethernet 1000BaseT mit Kabel der Kategorie CAT5e)](https://www.juergarnold.ch/Netzwerkverkabelung/verbindung4.jpg)

### 4-Kanal-HiSpeed (Voll)-Duplex → Bidirektional (z.B.: Ethernet 10GBaseT mit Kabel der Kategorie CAT7)

- 4-Kanal HiSpeed Voll-Duplex ermöglicht gleichzeitige bidirektionale Kommunikation
    - Datenübertragungen erfolgen über vier separate Hochgeschwindigkeitskanäle
    - Geräte können gleichzeitig senden und empfangen, ohne Kollisionen zu verursachen
    - Verwendet in Ethernetverkabelungen wie 10GBaseT mit Kabel der Kategorie CAT7
    - Bietet höhere Bandbreite und geringere Latenzzeit als 2-Kanal und 4-Kanal Voll-Duplex
    - Effektive Methode für schnelle und zuverlässige bidirektionale Kommunikation in Netzwerken und anderen Anwendungen, die eine sehr hohe Bandbreite erfordern.

![4-Kanal-HiSpeed (Voll)-Duplex → Bidirektional (z.B.: Ethernet 10GBaseT mit Kabel der Kategorie CAT7)](https://www.juergarnold.ch/Netzwerkverkabelung/verbindung5.jpg)

- [Wikipedia | Duplex](https://de.wikipedia.org/wiki/Duplex_(Nachrichtentechnik))

## 1.1 Netzwerktopologie

Die Topologie bezeichnet bei einem Computernetz die Struktur der Verbindungen mehrerer Geräte untereinander, um einen gemeinsamen Datenaustausch zu gewährleisten.

![Topologie](https://www.juergarnold.ch/Netzwerkverkabelung/topologie.jpg)

### Bustopologie

- Vorteile:
    - Einfache und kostengünstige Methode zur Verbindung von Geräten in einem Netzwerk
    - Kein zentraler Steuerpunkt erforderlich
    - Flexibles Netzwerkdesign, da Geräte einfach hinzugefügt und entfernt werden können

- Nachteile:
    - Anfällig für Kollisionen, wenn mehrere Geräte versuchen, gleichzeitig Daten zu senden
    - Schwierig zu verwalten und zu diagnostizieren, wenn Probleme auftreten
    - Begrenzte Entfernung zwischen den Geräten, da das Signal auf dem Bus abnimmt
    - Geringere Bandbreite und höhere Latenzzeiten als andere Topologien wie Stern oder Ring.

### Sterntopologie

- Vorteile:
    - Einfache Verwaltung und Diagnose von Netzwerkproblemen
    - Geringeres Risiko von Kollisionen, da jedes Gerät eine separate Verbindung zum zentralen Knotenpunkt hat
    - Flexibles Netzwerkdesign, da Geräte einfach hinzugefügt oder entfernt werden können
    - Höhere Bandbreite und schnellere Datenübertragungsraten als bei Bustopologie

- Nachteile:
    - Höhere Kosten aufgrund der Notwendigkeit von zentralen Knotenpunkten
    - Ausfall des zentralen Knotenpunkts führt zum Ausfall des gesamten Netzwerks
    - Begrenzte Entfernung zwischen Geräten, da das Signal auf dem Kabel abnimmt
    - Erhöhter Verkabelungsaufwand im Vergleich zur Bustopologie.

### Baumtopologie

- Vorteile:
    - Ermöglicht eine effiziente Verwaltung von großen Netzwerken
    - Skalierbarkeit, da zusätzliche Zweige hinzugefügt werden können
    - Redundanz durch die Verwendung von mehreren Pfaden zwischen verschiedenen Zweigen
    - Geringere Kollisionen als bei der Bustopologie

- Nachteile:
    - Ausfall des Wurzelknotens kann das gesamte Netzwerk beeinträchtigen
    - Höhere Kosten aufgrund der Notwendigkeit von zentralen Knotenpunkten und mehreren Verbindungslinien
    - Komplexität der Verwaltung und Diagnose von Netzwerkproblemen
    - Begrenzte Entfernung zwischen Geräten aufgrund von Signalverlusten auf dem Kabel.

### Ringtopologie

- Vorteile:
    - Gleichberechtigte Zugriffsrechte für alle Geräte
    - Weniger Kollisionen als bei der Bustopologie
    - Einfache Implementierung und Verwaltung des Netzwerks
    - Gleichmäßige Datenübertragungsraten auf allen Geräten

- Nachteile:
    - Ausfall eines Knotens kann das gesamte Netzwerk beeinträchtigen
    - Höhere Kosten aufgrund der Notwendigkeit von zentralen Knotenpunkten und mehreren Verbindungslinien
    - Begrenzte Entfernung zwischen Geräten aufgrund von Signalverlusten auf dem Kabel
    - Schwieriger zu erweitern als andere Topologien wie Baum oder Stern.

### Meshtopologie

- Vorteile:
    - Hohe Zuverlässigkeit und Redundanz, da jedes Gerät mehrere Verbindungen hat
    - Hohe Skalierbarkeit und Flexibilität, da neue Geräte einfach hinzugefügt werden können
    - Hohe Bandbreite und schnelle Datenübertragungsraten
    - Effektive Nutzung der Netzwerkressourcen durch optimale Routing-Protokolle

- Nachteile:
    - Höhere Kosten aufgrund der Notwendigkeit von mehreren Verbindungen und intelligenten Routing-Algorithmen
    - Komplexität der Implementierung und Verwaltung des Netzwerks
    - Begrenzte Entfernung zwischen Geräten aufgrund von Signalverlusten auf dem Kabel
    - Hoher Energiebedarf für die drahtlose Übertragung von Daten.

Historisches Ethernet entspricht einer Bus-Topologie (z.B. Yellow-Cable). Aktuelles Ethernet entspricht einer Stern/Baum-Topologie.
![Historisches Ethernet](https://www.juergarnold.ch/Netzwerkverkabelung/Ethernettopologie.jpg)
Und so werden in einem modernen Netzwerk die Geräte (PC, Server, Switch etc.) untereinander verbunden: (Zum Thema Switch, Router und xDSL-Modem in einem anderen Beitrag mehr.)
![Switch_Router](https://www.juergarnold.ch/Netzwerkverkabelung/Switch_Router.jpg)

# 2.0 Das Ethernetkabel
## 2.1 Draht, Litze oder Glasfaser?

![leiterarten](https://www.juergarnold.ch/Netzwerkverkabelung/leiterarten.jpg)

- Draht:
    - Starr, bricht bei mehrmaligem Biegen und muss darum in einem Kabelkanal geführt (fixiert) verlegt werden.
    - Billiger in der Produktion.
    - Vorteile dank Stabilität beim Verlegen (Durchstossen) durch Kabelkanäle.
    - Lässt sich nicht crimpen (Siehe RJ45-Steckerherstellung).
    - Kann bei Steckdosenmontage (Schneidklemmen, Schraubverbindungen) verwendet werden.
    - Verwendung in UGV-Verkablungen: Universelle Gebäudeverkabelung = Strukturierte Verkabelung in Gebäuden, Büros etc.

- Litze:
    - Flexibel, bricht nicht bei mehrmaligem Biegen und kann darum lose verlegt werden.
    - Aufwändigere Produktion (Verseilung) und darum etwas teurer.
    - Verlegen durch Kabelkanäle kaum möglich.
    - Lässt sich crimpen (Siehe RJ45-Steckerherstellung).
    - Kann bei Steckdosenmontage (Schneidklemmen, Schraubverbindungen) nicht verwendet werden.
    - Verwendung bei Patch-Kabeln: Verbindung von Steckdose zu Endgerät wie PC/Server oder Netzwerkkomponente wie Switch, Router etc.

- Glasfaser:
    - Eher starres Medium, wo bei zu engem Biegeradius die Übertragungsleistung leidet (Diffuse Zone) oder das Glas vollständig bricht.
    - Billiger in der Produktion aber teuer (Spezialgeräte jenseits von Crimpzangen) in der Endverarbeitung (Steckermontage).
    - Eine Lichtverbindung lässt prinzipiell eine hohe Datenübertragung zu, erfordert aber auch eine entsprechend leistungsfähige (schnelle) Elektronik. Darum: Performance ist, wie bei den Kupferverbindungen, vom spezifizierten Ethernetstandard abhängig.

## 2.2 Konzepte zur Abwehr von Störeinflüssen

Einige potentielle Störungsquellen kommen in Frage: Benachbarte Hochspannungsanlagen, Motoren, Eisenbahnanlagen, Leistungsschalter, Antennen, atmosphärische Störungen, etc. Aber auch das benachbarte Aderpaar im selben Kabel kann Störeinflüsse bewirken.

![emv](https://www.juergarnold.ch/Netzwerkverkabelung/emv.jpg)
Grossem Störpotential ausgesetzt sind z.B. Datenkabel, die durch Industrieanlagen (Maschinenpark in der Werkstatt), bahntechnische Anlagen, Elektrizitätswerke etc. geführt werden. Dort muss besonders auf genügenden Störschutz geachtet werden.

- Abschirmung: [Shielding/Screening] Der Faradaysche Käfig ist eine allseitig geschlossene und geerdete Hülle aus einem elektrischen Leiter (z. B. Drahtgeflecht oder Blech), der als elektrische Abschirmung wirkt. (Im Deutschen Museum in München bietet sich die Gelegenheit, am eigenen Körper einen abgewehrten Blitzeinschlag zu erleben. Die Person ist durch ein umhüllendes und geerdetes Metallgitter, dem faradayschen Käfig, vor dem Blitzeinschlag geschützt.)

![Faraday](https://www.juergarnold.ch/Netzwerkverkabelung/Faraday.jpg)
- Verdrillte Leitungspaare: [Twisted Pair] Störungen wirken auf beide Drähte gleichzeitig und heben sich dank der verdrillten Paar-Anordnung gegenseitig auf
![Verdrillen](https://www.juergarnold.ch/Netzwerkverkabelung/Verdrillen.jpg)

## 2.3 Die Ethernet-Kabelkategorien

Im Handel werden die Ethernetkabel nach Kabelkategorien angeboten:
![CAT7Bsp](https://www.juergarnold.ch/Netzwerkverkabelung/CAT7Bsp.jpg)
Umso höher die Kabelkategorie, umso besser (höhere Bandbreite) das Kabel. Der Kabelaufbau kann sich dabei unterscheiden. (Die genauen Angaben entnehme man der Kabelspezifikation.)

Screened, Shielded, Foiled und Twisted-Pair: Die Kupfergeflechtabschirmung/Braid Shield oder Folienabschirmung/Foiled Shield schirmen Störungen ab und wirken wie ein Faradayscher Käfig, wenn Störeinflüsse ausserhalb des Kabels auftreten. Allerdings kann die Störquelle auch innerhalb des Kabel liegen, nämlich dann, wenn sich hochfrequente Signale in einem Kabel gegenseitig stören.
![Abwehrmassnahmen](https://www.juergarnold.ch/Netzwerkverkabelung/Abwehrmassnahmen.jpg)

- Screened: Kupfergeflechtabschirmung über das ganze Kabel gegen niederfrequente Störungen
- Shielded: Kupfergeflechtabschirmung über die verdrillten Aderpaare gegen niederfrequente Störungen
- Foiled: Folienabschirmung gegen hochfrequente Störungen über das ganze Kalbel oder über Aderpaare.
- Twisted Pair: Verdrillte Aderpaare für die Unterdrückung von Gleichtaktstörungen. Unter Gleichtaktstörungen werden Störspannungen auf der Übertragungsleitung verstanden, welche sich mit gleicher Phasenlage und Stromrichtung sowohl auf der Hinleitung als auch der Rückleitung ausbreiten. Durch die Verdrillung können diese Störungen stark reduziert werden.

Die folgenden Bilder zeigen gängige Ethernet-Kabelaufbauten. Die zuvor besprochenen Abwerhmassnahmen sind entweder teilweise, komplett oder gar kombiniert implementiert:

![kabelarten](https://www.juergarnold.ch/Netzwerkverkabelung/kabelarten.jpg)

- U=Unshielded (Ungeschirmt)
- S=Shielded oder Screened (Geflechtschirm, gegen niederfrequente Störungen)
- F=Foiled (Folienschirm, gegen hochfrequente Störungen)
- SF=Screened & Foiled
- TP=Twisted Pair (Verdrillte Aderpaare)
______________________

- Screened: Schirm über das ganze Kabel
- Shielded: Schirm über die verdrillten Aderpaare

Beispiel:
- S/STP = Screened / Shielded Twisted Pair
- SF/STP = Screend-Foiled / Shielded Twisted Pair

## 2.4 Die Ethernet-Medientypen

Die verschiedenen Ethernet-Standards unterscheiden sich in Übertragungsrate, der Topologie, den verwendeten Kabeltypen (Medium) und der Leitungskodierung.

Material/Medium: Kupfer
- 10Base-5: DIX(1980)/802.3(1983); Bus/Halfduplex; 10Mbps; - 1x50Ω Koaxialkabel/Thick-Ethernet; Segmentlänge ≤ 500m
- 10Base-2: 802.3a(1985); Bus/Halfduplex; 10Mbps; 1x50Ω RG58-Koaxialkabel/Thin-Ethernet; Segmentlänge ≤ 185m
- 10Base-T: 802.3i(1990); Stern/Duplex; 10Mbps; 2x100Ω Aderpaare ≥ CAT3/UTP-Kabel; Segmentlänge ≤ 100m
- 100Base-TX: 802.3u(1995); Stern/Duplex; 100Mbps; 2x100Ω Aderpaare ≥ CAT5/UTP-Kabel; Segmentlänge ≤ 100m
- 100Base-T2: 802.3y(1997); Stern/Duplex; 100Mbps; 2x100Ω Aderpaare ≥ CAT3/UTP-Kabel; Segmentlänge ≤ 100m
- 1000Base-T: 802.3ab(1999); Stern/Duplex; 1Gbps; 4x100Ω Aderpaare ≥ CAT5e; Segmentlänge ≤ 100m
- 10GBase-T: 802.3an(2007); Stern/Duplex; 10Gbps
______________________
- 4 Aderpaare CAT6e → Segmentlänge ≤ 55m
- 4 Aderpaare CAT6a/CAT7 → Segmentlänge ≤ 100m

Material/Medium: Glasfaser/Fibre
- 100Base-FX: 802.3u(1995); Stern/Duplex; 100Mbps; 2xGlasfaser; Segmentlänge ≤ 2000m
- 1000Base-LX: 802.3z(1998); Stern/Duplex; 1Gbps; Long-Wavelength (1300nm)
50μm / 62.5μm Multimode → Segmentlänge ≤ 550m
10μm → Segmentlänge ≤ 5000m
- 1000Base-SX: 802.3z(1998); Stern/Duplex; 1Gbps; Short-Wavelength (850nm)
62.5μm Multimode → Segmentlänge ≤ 275m
50μm Multimode → Segmentlänge ≤ 550m

## 2.5 Ethernetkabel im Eigenbau mit RJ45

Benötigt man Ethernet-Patchkabel, findet man im Elektrohandel fertig konvektioniertes Kabelmaterial in verschiedenen Längen und Qualitätsstufen. Will man sein eigenes und auf seine speziellen Bedürfnisse zugeschnittes Kabel herstellen, braucht es neben dem Kabelmaterial in der geeigneten Kabelkategorie auch noch die beiden RJ45 Stecker, und zwar je nach Situation die ungeschirmte oder geschirmte Version. Meist muss man die Steckerschutzkappe separat dazu kaufen. Dabei ist die Version zu empfehlen, die die RJ45-Steckerrastnase hinreichend schützt. Schlussendlich muss für das Crimpen der Steckerkontakte eine RJ45-Crimpzange verfügbar sein. Es muss ein guter Grund geben, ein Ethernetkabel selber herzustellen. Nur der Preis kann es kaum sein. (Arbeitsaufwand beim Eigenbau mitberücksichtigen)

- RJ45-Steckerbelegung: Je nach Ethernet-Medientyp werden beim 8-poligen RJ45-Stecker 4 oder 8 Kontakte belegt.
Beispiel-1: 100 BASE TX besteht aus TX-Leiterpaar (T=Transmit) und RX-Leiterpaar (R=Receive)
Beispiel-2: 1000 BASE T besteht aus 4 Leiterpaaren (D1+, D1-, D2+, D2-,D3+, D3-,D4+, D4-), welche die Daten Bi-Direktional übertragen.

![rj45belegung](https://www.juergarnold.ch/Netzwerkverkabelung/rj45belegung.jpg)

Eine kleine Anekdote zu der etwas eigenartigen Verdrahtung des RJ45-Steckers: Wie aus dem obigen Bild ersichtlich, schiebt sich das blaue Aderpaar zwischen das Grüne. Elektrisch gesehen sicher kein Mehrwert. Woher kommt es dann? Wie oft bei solchen Dingen, ist dies mit einer historischen Entwicklung zu erklären: Es war einmal die Telefon-Zweidrahtleitung. Beim Verdrahten des 2-poligen Telefonsteckers bzw. genormten Buchse oder auf Englisch Registered Jack, kurz RJ, musste der Elektriker sich nicht allzusehr darum kümmern, welche Drähte in welche Buchsen gestopft werden – es funktionierte immer. Als man dann zwei Telefongespräche über ein einziges Kabel schicken wollte, musste man nur um zwei Drähte aufstocken. Um sich nicht plötzlich über Falschverpolung zu sorgen, wurden die beiden Drähte einfach ausserhalb des ursprünglichen Kabelpaars angeordnet und man konnte den Stecker sozusagen unbeschwert auch verkehrt einstecken.  Als dann Ethernet in den Büros Einzug hielt, damals nur auf zwei Aderpaare beschränkt, wollte man Telefon und Ethernet über dieselbe Leitung betreiben, also fügte man links und rechts der bestehenden vier Verbindung je ein Aderpaar hinzu. Der 8-polige Stecker war erfunden und ist auch heute noch unter der Bezeichnung RJ45 bekannt. Dank der speziellen Verbindungsanordnung ist aber immer noch der «alte» Telefonstecker RJ11 ohne Probleme in eine RJ45-Buchse «mittig» einsteckbar. Das analoge Telefon hat ja bekanntlich zugunsten der IP-Telefonie ausgedient, die schräge Kabelanordnung ist aber geblieben und wenn sie nicht gestorben sind, dann leben sie noch heut‘.

- RJ45-Stecker crimpen: Dabei ist folgendes zu beachten: Die Kabel müssen aus Litze sein, sonst funktioniert das Crimpen nicht. Beim Einführen der Litzendrähte in die Steckerpins unbedingt die Farbreihenfolge gem. z.B. Farbschema T568B beachten. Die Litzen werden dabei nicht abisoliert, sondern stumpf in die Steckerhülsen eingeschoben. Zum Schluss das fertige Kabel gründlich testen.

![crimpen](https://www.juergarnold.ch/Netzwerkverkabelung/crimpen.jpg)
Geschirmte Kabel wie z.B. das S-UTP-Kabel (Screened Unshielded Twisted Pair) benötigen RJ45-Stecker mit Abschirmung. Man erkennt diese an den spiegelnden Oberflächen. Bei ungeschirmten Kabeln wie das UTP-Kabel (Unshielded Twisted Pair) genügen RJ45-Stecker ohne Abschirmung. Man erkennt diese an ihren Oberflächen aus durchsichtigem Kunststoff.
![kabelaufbau](https://www.juergarnold.ch/Netzwerkverkabelung/kabelaufbau.jpg)
- RJ45-Steckdosenherstellung: Die RJ45-Steckdosen oder Buchsen sind Teil der Universellen Gebäudeverkabelung (UGV), die mit Ethernet-Drahtkabel ausgeführt wird. Je nach Anwendung sind verschiedene Ausführungen erhältlich:

![Steckdose](https://www.juergarnold.ch/Netzwerkverkabelung/Steckdose.jpg)

- Straight Through oder Crossover? Grundsätzlich muss bei einer bidirektionaler Verbindung (Duplex) wie diesem Telefonbeispiel die Sende- mit der Empfangsleitung ausgekreuzt (Crossover) werden.

![Crossover1](https://www.juergarnold.ch/Netzwerkverkabelung/Crossover1.jpg)
Wird die Kommunikation über mehrere Vermittlungsstellen geleitet, muss darauf geachtet werden, dass das Signal nur einmal ausgekreuzt wird.
![Crossover2](https://www.juergarnold.ch/Netzwerkverkabelung/Crossover2.jpg)
Moderne Kommunikationssgeräte (Switch) sind in der Lage zu erkennen, ob das Signal ausgekreuzt werden muss oder nicht. Die entsprechende Eigenschaft eines Switches nennt man Auto-MDI-X. Benutzt man solche Switches, spielt es keine Rolle, welchen Kabeltyp (gekreuzt/ungekreuzt) man verwendet. Im anderen Fall hat man selber für das Auskreuzen der Verbindungen zu sorgen, indem man Straight-Through- oder Crossover-Ethernetkabel einsetzt.

![Crossover3](https://www.juergarnold.ch/Netzwerkverkabelung/Crossover3.jpg)

# 3.0 Universelle Gebäudeverkabelung

Die UGV (Universelle Gebäude Verkabelung), auch «Strukturierte Verkabelung» genannt, stellt einen einheitlichen Aufbauplan für Verkabelungen dar und ist Teil der technischen Infrastruktur einer Liegenschaft.

- Der Primärbereich ist die Verkabelung der Gebäude eines Standortes untereinander und wird auch als Campusverkabelung bezeichnet.
- Der Sekundärbereich ist die vertikale Stockwerkverkabelung, also die Verkabelung der Stockwerke eines Gebäudes untereinander und wird auch als Steigbereichverkabelung oder als Gebäudeverkabelung bezeichnet.
- Der Tertiärbereich ist die horizontale Stockwerkverkabelung, also die Verkabelung innerhalb der Stockwerke eines Gebäudes und wird auch als Etagenverkabelung bezeichnet.
- Patchkabel für die Rangierungen zwischen Patchpanels oder Patchpanel und Switch's oder Anschlusssteckdose und Endgerät. Patchkabel bestehen immer aus Litze, wogegen bei der festinstallierten UGV-Verkabelung ausschliesslich Draht zur Anwendung kommt.
- Patchpanels für Kupfer- und Glasfaserkabel sind verschieden grosse Verteilerfelder (Rangierfelder) und stellen je nach benötigter Menge entsprechend viele Anschlüsse (RJ45-Anschlussdosen) zur Verfügung. Das UGV-Kabel führt immer von einer RJ45-Steckdose zu einer RJ45-Steckdose!
![NetworkCabeling](https://www.juergarnold.ch/Netzwerkverkabelung/NetworkCabeling.jpg)

## 3.1 Das Logisches Layout und der Verkabelungsplan

In der Netzwerktechnik unterscheidet man zwei Arten von Topologien: Die logische und physische Topologie. Man spricht auch von logischem Layout und Verkabelungsplan womit das physikalische Layout gemeint ist. Das logisches Layout hat mehr den Aspekt eines logischen Aufbaus und soll aufzeigen, welche Komponenten (PC, Server, Switch, Router) wie miteinander verbunden sind. Die Kabelführung, UGV (Universelle Gebäudeverkabelung) oder Patchkabel ist hier nicht von Bedeutung. Anders sieht das beim Verkabelungsplan aus. Hier ist die Verkabelung anhand eines Gebäudegrundrisses dokumentiert. Es soll daraus auch ersichtlich sein, ob die Verbindung als UGV oder Patchkabel realisiert wird. Generell gilt: Festinstallationen werden als UGV ausgeführt und zwar von Netzwerksteckdose zu Netzwerksteckdose. Die Komponenten werden mit Patchkabeln an die Netzwerksteckdosen (oder Patchpanels) angeschlossen. Der Gebäudeverkabelungsplan soll dem Installateur aufzeigen, wie er das Gebäude zu verkabeln hat. Wichtig ist, dass logisches Layout und Verkabelungsplan übereinstimmen bezüglich Anschluss und Beschriftung der Komponenten.
Im folgenden ein Beispiel:

![LogPhyLayout](https://www.juergarnold.ch/Netzwerkverkabelung/LogPhyLayout.jpg)
