# Aufgabenteil 12
1. Bestimmen sie von der folgenden IP-Adresse die Netz-ID und Host-ID: 
192.168.3.37/24

        Subnetzmaske:
        /24 oder 255.255.255.0
        /16 oder 255.255.0.0

        Netz-ID: 192.168.3
        Host-ID: 37

2. Geben sie für die folgende IP-Adresse die Netzwerkadresse und Broadcastadresse 
an: 78.23.49.123 / 255.255.255.0

        Netzwerkadresse : 78.23.49.0
        Boradcastadresse : 78.23.49.255

3. Wie beurteilen sie diese Adresse: 78.256.125.12 / 255.255.248.0

        256 ist eine ungültige Netzwerkadresse
        (Only 0 bis 255)

4. Können sie ihrem PC die Adresse 172.30.0.0 vergeben?

        Es ist nicht möglich da das die Netzwerkadresse ist.

5. Ihr PC hat folgende Netzwerkeinstellungen:
IP: 10.23.65.128 / 16
Standardgateway: 10.24.0.1 / 16
Wie beurteilen sie diese Situation?

        Standardgateway ist der Router und muss im selben 
        Netz sein, wie der PC. 
        Ist er aber in diesem Beispiel nicht.

6. Welche der beiden Adressen ist eine aus dem privaten Adressbereich: 
10.255.255.254 oder 172.25.123.4

        Beide stehen im privatem Adressbereich

7. Wie beurteilen sie diese IP-Adresse: 169.254.0.1 / 16

        APIPA-Adresse 
        (Adressen automatisch beziehen 
        aber kein DHCP-Server verfügbar)

8. Wann verwenden sie diese IP-Adresse: 127.0.0.1

        Local Host: Wenn sie versuchen etwas lokal zu 
        testen oder in der Art probieren.

9. Die beiden folgenden PCs sind über einen Switch verbunden. Die IP-Adressen 
lauten: 172.16.3.48/24 und 172.16.4.126/24
Können sich die beiden PCs gegenseitig anpingen?

        Nein, sie stehen nicht im gleichen Netz
        (Verschiedene Subnetz)

10. Wie beurteilen sie diese Adresse: 172.22.17.201 mit Subnetzmaske 255.0.0.0

        Wird nicht funktionieren, da die Subnetzmaske 
        folgendes zulässt: 172.0.0.0 bis 172.255.255.255.
        Damit überschreitet man aber den privatem in den
        öffentlichen Bereich.

11. Sowohl ein Switch wie auch ein Router muss das eintreffende IP-Paket analysieren.
Welche Angaben benötigt der Switch und welche der Router?

        Switch: Mac-Adresse
        Router: Mac-Adresse und IP-Adresse

12. Ihr Hackerfreund prahlt damit, dass es ihm gelungen sei, die MAC-Adresse seiner 
Netzwerkkarte zu ändern und damit eine Node-Locked-License zu missbrauchen.
Zum Beweis hat er dieselbe MAC-Adresse gewählt, wie die ihre.
Was ist das Problem dabei und wann könnte der Hack keinen Einfluss haben

        Pakete sind im eigene Subnetzmaske nicht einstellbar
