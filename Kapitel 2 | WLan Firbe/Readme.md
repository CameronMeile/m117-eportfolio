# Kapitel 2 | WLan Fibre

### INDEX:

[TOC]

# WLAN → Wireless Local Area Network

### ISP
Internetdienstanbieter oder Internetdienstleister (englisch Internet Service Provider, abgekürzt ISP oder Internet Access Provider), im deutschsprachigen Raum auch oft nur Provider, umgangssprachlich meist nur Internetanbieter oder Internetprovider genannt, sind Anbieter von Diensten, Inhalten oder technischen Leistungen, die für die Nutzung oder den Betrieb von Inhalten und Diensten im Internet erforderlich sind.

- [ISP **Sunrise 1Gbit's**](https://www.sunrise.ch/asset/x/45fe12efce/w1_factsheet-sunrise-up-internet-l_de.pdf) | Bestseller 79CHF
- [ISP **Swisscom 1Gbit's**](https://www.swisscom.ch/de/privatkunden/internet/internet-abo.html/) | Bestseller 69.90CHF

Das WLAN ist ein drahtloses lokales Funknetzwerk.

![WlanSet](https://www.juergarnold.ch/Netzwerkerweiterung/WLANset.jpg)

WDS (Wireless Distribution Repeater)
Ein als WDS konfigurierter Access Point ist eine WLAN-Basisstation, die schwache Funksignale empfängt, neu aufbereitet und verstärkt wieder abstrahlt. WLAN-Repeater vergrössern im Prinzip die Reichweite einer einzelnen Basisstation, die sie über ihre Hardware-Adresse (MAC) identifizieren. Bei der Repeater-Funktion handelt es sich praktisch um eine Funkverlängerung. Der WLAN-Repeater verteilt dabei die Datenpakete per Broadcast an alle WLAN-Teilnehmer und erzeugen damit eine Datenflut im WLAN. AP und Repeater haben dieselbe SSID, denselben WPA/WEP-Schlüssel und denselben Funkkanal. Da der AccessPoint und Repeater die gleiche SSID haben, können sich die WLAN-Clients wahlweise mit dem Repeater oder dem Access Point verbinden. Je nachdem, welches Funksignal stärker ist. WDS gibt es als Single-Radio-WDS (Ein Kanal für Weiterleitung an benachbarte AP’s und Clients) und Dual-Radio-WDS (Zwei Kanäle, davon einer für Weiterleitung an AP’s und ein weiterer für die Clients. Dieses Verfahren ist daher effizienter)

# WLAN-Sicherheit

- Die Sicherheit von WLANs basiert auf einer Kombination aus Authentifizierung und Verschlüsselung.
- Ein offenes WLAN stellt sich wie ein offenes Scheunentor dar. Beim Surfen über das offene WLAN hinterlässt die IP-Adresse des WLAN-Betreibers eine Spur im Netz. Diese IP-Adresse kann im nachhinein dem Anschlussinhaber zugeordnet werden. Der Anschlussinhaber wird daher im Rahmen einer Rechtsverletzung als erster Verdächtiger ermittelt.
- Wenn immer möglich mit WPA2 (WIFI Protected Access 2 – AES/CCMP/IEEE802.11i) verschlüsselt verbinden – WPA2 gilt als hinreichend sicher, wenn das WLAN-Passwort möglichst lang und komplex ist – Befindet sich das Passwort in einem Wörterbuch, dann bestehen gute Chancen, dass ein Angreifer das Passwort herauszufinden kann.
- WEP ist geknackt und sollte nicht mehr verwendet werden.
- Im kommerziellen oder großräumigen Einsatz sollte ein WLAN immer im Enterprise Mode mit IEEE 802.1x gesichert werden.
WPS ausschalten.
- Die WLAN-Reichweite bzw. Antenne und Sendeleistung den Bedürfnissen anpassen um ein «Mithören» nicht zu fördern.
- Default-Admin-Passwort ändern.
- Aktuelle Patches einspielen.
- SSID wählen, die keine Rückschlüsse auf die Firma oder Namen zulässt.
- MAC-Adressfilter einsetzen ist fraglich – Ein Hacker wird sich einfach seinen WLAN-Adapter mit der MAC-Adresse eines berechtigten  WLAN-Adapters überschreiben und schon ist der MAC-Filter umgangen.
- SSID-Broadcast verstecken ist fraglich und wird auch nicht offiziell unterstützt bzw. ist für Hacker kein wirkliches Hindernis, denn sobald sich ein Client an einem WLAN mit versteckter SSID anmeldet, wird dabei die SSID übertragen)
(Es entsteht sogar eine Sicherheitslücke: Normalerweise würde der WLAN-Access-Point regelmässig über sein WLAN informieren. Wenn er das nicht tut, dann muss der WLAN-Client, der einmal damit verbunden war, aktiv nach diesem WLAN suchen. Deshalb broadcastet dieser Client regelmäßig die SSID von sich aus heraus. Er ruft praktisch ständig nach dem versteckten WLAN. Auch wenn das gar nicht in der Nähe ist.)
- WLANs von anderen Netzwerk-Segmenten logisch trennen.
- Firewall zwischen WLAN und LAN.
- VPN einsetzen.
- IDS (Intrusion Detection System) im WLAN aufstellen, um Angriffe zu erkennen.
- Regelmässige Audits mit aktuellen Hacker-Tools.
- Netzzugang regeln mit z.B. Radius-Server.
- Wie man ein WLAN «hacken» kann, wird z.B. ausführlich auf www.elektronik-kompendium.de beschrieben. Stichwort: «WLAN-Hacking» oder hier der Direktlink.

# WLAN-Frequenzen und Kanäle

Für WLAN im Frequenzband 2.4GHz sind 13 Kanäle 1-13 nutzbar. Kanalabstand: 5MHz
Für WLAN im Frequenzband 5GHz sind 24 Kanäle 36,40,44,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136, 140,149,153,157,161,165 nutzbar. Kanalabstand: 20MHz
Die Kanalanzahl bzw. nutzbare Kanäle sind regional und auch überregional sehr unterschiedlich.

Eine Videoerklärung zum Thema WLAN Frequenzen und Kanalüberlappung kann hier heruntergeladen werden: WLANFrequenzenKanaele.mp4

### Beispiel 2.4GHz:

![kANAL](https://www.juergarnold.ch/Netzwerkerweiterung/Kanal.jpg)

Sowohl der 2.4GHz- wie auch im 5GHz Bereich ist in verschiedene Kanäle unterteilt. Ein Kanal hat eine Bandbreite von 20MHz, 22MHz oder 40MHz. Der AP sendet auf genau einem dieser Kanäle. Dies kann man am AP entsprechend konfigurieren. Benachbarte AP’s sollten aber nicht denselben Kanal benutzen, sonst kommt es zu einer Kanalüberlappung.

- Bei 13 Kanälen (Europa) sind je nach Bandbreite (20/22/40MHz) jeweils 2, 3, max. 4 Kanäle ohne Überlappung nutzbar.
- Falls mehrerer Access Points geplant sind ist darauf zu achten, dass die genutzten Kanäle nicht zu dicht beieinander liegen.
- Eine Kanalüberlappung ist unbedingt zu vermeiden, weil dies sonst zu einer geringeren Übertragungsrate führen würde.
- Eine Kanalüberlappung ist nur bei AP’s zulässig, die mindestens 30 Meter auseinander liegen.
- Die Bandbreite, je nach IEEE802.11-Norm 20MHz, 40MHz. 80MHz oder 160MHz kann bei professionellen APs eingestellt werden und wird bei einfacheren APs zwischen den Teilnehmern ausgehandelt. Je höher die Bandbreite, umso weniger verfügbare bzw. überlappungsfreie Kanäle, dafür potentionell höhere Datenübertragungsrate. (Abhängig von weiteren Kriterien)
- Ausserdem muss jeder PC die Übertragungsleistung auf dem selben Kanal mit anderen PC’s teilen (Shared Medium/Kollisionsdomäne)

Kanalwahl fix oder dynamisch? Der Accesspoint prüft die verfügbaren Kanäle und wählt den aus, den ihm am vorteilhaftesten erscheint, weil darauf am wenigsten Traffic messbar ist. Das ist sinnvoll und auch die einzige Möglichkeit an Orten, wo eine hohe WLAN-Dichte besteht und man kein Einfluss auf benachbarte APs hat. Auf dem eigenen Campus hat man aber die Möglichkeit, die Kanäle bezüglich den geografischen Gegebenheiten sorgfältig zu planen und fix zuzuteilen. Dagegen spricht dann allerdings wieder, dass wenn ein Mitarbeiter mit seinem z.B. Smartphone einen eigenen HotSpot eröffnet, es zu Interferenzen kommen kann, weil die fix eingestellten APs frequenzmässig nun nicht mehr ausweichen können.
