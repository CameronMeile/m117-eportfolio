# Modul 117 ePortfolio

### INDEX
0. Einleitung
1. [Kapitel 1 | Netzwerkverkabelung (Ethernet)](Kapitel 1 | Netzwerkverkabelung/Readme.md)
2. [Kapitel 2 | Wlan Fibre](Kapitel 2 | WLan Firbe/Readme.md)
3. [Kapitel 3 | IP](Kapitel 3 | IP/Readme.mdP)
4. [Kapitel 4 | TCPIP](Kapitel 4 | TCPIP/.gitkeep)
5. [Kapitel 5 | IP-Praxis](Kapitel 5 | IP_Praxis/Teil5_IP_Praxis.pdf)
6. [Kapitel 6 | Benutzer Berechtigungen](Kapitel 6 | Benutzer Berechtigungen/Teil6_Benutzer_Berechtigungen.pdf)
7. [Kapitel 7 | WINinstall](Kapitel 7 | WINinstall/Teil7_WINinstall.pdf)
99. Zusammenfassung
100. Quellen

# 0.0 Einleitung

In diesem Modul sollen Sie ein ePortfolio mit folgendem Inhalt führen:
- Theoriezusammenfassung der einzelnen Kapitel
- Arbeitsaufträge, Lösungen zu den Aufgaben (Immer auch mir der Aufgabenstellung!)
- Unterrichtsreflexion, Lernjournal
Ihr ePortfolio soll eine geeignete, klare Dateistruktur aufweisen.

Sie sollen Ihr ePortfolio stets aktuell halten bzw. auf den darauffolgenden Unterrichtstag ergänzt haben. (Gilt auch für Abwesende) Das Fächertagebuch gibt über den jeweiligen Unterrichtsverlauf bzw. Inhalt Auskunft.

# 1.0 Kapitel 1 | Netzwerkverkabelung (Ethernet)

- 1.0 Simplex, Halfduplex und Duplex
    -   1.1 Netzwerktopologie
- 2.0 Das Ethernetkabel
    - 2.1 Draht, Litze oder Glasfaser
    - 2.2 Konzepte zur Abwehr von Störeinflüssen
    - 2.3 Die Ethernet-Kabelkategorien
    - 2.4 Die Ethernet-Medientypen
    - 2.5 Ethernetkabel im Eigenbau mit RJ45
- 3.0 Universelle Gebäudeverkabelung
    - 3.1 Das Logisches Layout und der Verkabelungsplan

# 2.0 Kapitel 2 | Wlan Fibre

- 1.0 WLAN → Wireless Local Area Network
    - 1.1 WLAN-Betriebsarten
    - 1.2 WLAN-Sicherheit
    - 1.3 WLAN-Frequenzen und Kanäle
    - 1.4 WLAN-Funkantennen
    - 1.5 WLAN-Fehlersuche und Korrektur
    - 1.6 Dämpfung von WLAN-Funkwellen
    - 1.7 Die WLAN-Norm IEEE 802.11
    - 1.8 WLAN abhören
    - 1.9 WLAN-Begriffe und Abkürzungen

# 3.0/4.0 Kapitel 3/4 | IP / TCPIP

- 1.0 Die Netzwerkadressierung
    - 1.1 Der Hostname
    - 1.2 Die IP-Adresse (Logische Adresse)
    - 1.3 Die MAC-Adresse (Physikalische Adresse)
    - 1.4 Das Medienzugriffsverfahren CSMA/CD
    - 1.5 Divide et impera
    - 1.6 Die Subnetzmaske
    - 1.7 Subnetze verbinden
    - 1.8 Öffentliche und private IPv4-Adressbereiche
    - 1.9 Die Netzwerk- und Broadcastadresse
- 2.0 Der Internetzugang
    - 2.1 Der xDSL-Router
    - 2.2 Ein Webseitenaufruf
- 3.0 Zusammenfassung «Netzwerkadressierung»
- 4.0 IPv6 (Nachfolger von IPv4)
- 5.0 Netzwerkprotokolle als Schichtenarchitektur
    - 5.1 Das OSI-Referenzmodell
    - 5.2 Ein Webseitenaufruf aus Layer-Sicht
